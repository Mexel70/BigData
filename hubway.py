from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.exceptions import AirflowException

import airflow
from datetime import datetime
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator

from airflow.operators.http_download_operations import HttpDownloadOperator
from airflow.operators.zip_file_operations import UnzipFileOperator
from airflow.operators.hdfs_operations import *
from airflow.operators.filesystem_operations import CopyFileOperator, ClearDirectoryOperator, CreateDirectoryOperator
from airflow.operators.hive_operator import HiveOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.hive_operations import HiveToCsvOperator, CombineCsvOperator, AppendCsvOperator

import os

# DAG --------------------------------------------------------------------

args = {
    'owner': 'airflow'
}

dag = DAG(
        'Hubway',
        default_args=args,
        description='Hubway KPIs',
        schedule_interval='56 18 * * *',
        start_date=datetime(2019, 11, 11),
        catchup=False,
        max_active_runs=1
)


# List of files ----------------------------------------------------------

relevant_data_files = [
        '201501-hubway-tripdata.csv',
        '201502-hubway-tripdata.csv',
        '201503-hubway-tripdata.csv',
        '201504-hubway-tripdata.csv',
        '201505-hubway-tripdata.csv',
        '201506-hubway-tripdata.csv',
        '201507-hubway-tripdata.csv',
        '201508-hubway-tripdata.csv',
        '201509-hubway-tripdata.csv',
        '201510-hubway-tripdata.csv',
        '201511-hubway-tripdata.csv',
        '201512-hubway-tripdata.csv',
        '201601-hubway-tripdata.csv',
        '201602-hubway-tripdata.csv',
        '201603-hubway-tripdata.csv',
        '201604-hubway-tripdata.csv',
        '201605-hubway-tripdata.csv',
        '201606-hubway-tripdata.csv',
        '201607-hubway-tripdata.csv',
        '201608-hubway-tripdata.csv',
        '201609-hubway-tripdata.csv',
        '201610-hubway-tripdata.csv',
        '201611-hubway-tripdata.csv',
        # '201612-hubway-tripdata.csv', <- does not exist!
        '201701-hubway-tripdata.csv',
        '201702-hubway-tripdata.csv',
        '201703-hubway-tripdata.csv',
        '201704-hubway-tripdata.csv',
        '201705-hubway-tripdata.csv',
        '201706-hubway-tripdata.csv',
        '201707-hubway-tripdata.csv',
        '201708-hubway-tripdata.csv',
        '201709-hubway-tripdata.csv',
        '201710-hubway-tripdata.csv',
        '201711-hubway-tripdata.csv',
        '201712-hubway-tripdata.csv'
    ]


# Common stuff -----------------------------------------------------------


def get_date_str_from_file(filename):
    return filename.partition("-")[0]

def get_local_import_directory():
    return '/home/airflow/hubway'

def get_remote_data_directory(data_layer):
    return f'/user/hadoop/hubway/{data_layer}'

def get_local_kpi_directory(date_str):
    return f"/home/airflow/kpis/{date_str}"

def get_local_result_directory():
    return "/home/airflow/result"

def get_table_name_from_date_str(date_str):
    return f"trip_data_{date_str}"


# Create and clear target directories ------------------------------------


task_create_import_directory = CreateDirectoryOperator(
    task_id='create_import_directory',
    path='/home/airflow',
    directory='hubway',
    dag=dag,
)

task_clear_import_directory = ClearDirectoryOperator(
    task_id='clear_import_directory',
    directory=get_local_import_directory(),
    pattern='*',
    dag=dag,
)

task_create_kpi_directory = CreateDirectoryOperator(
    task_id='create_kpi_directory',
    path='/home/airflow',
    directory='kpis',
    dag=dag,
)

task_clear_kpi_directory = BashOperator(
    task_id='clear_kpi_directory',
    bash_command='rm -r /home/airflow/kpis',
    dag=dag
)

task_create_result_directory = CreateDirectoryOperator(
    task_id='create_result_directory',
    path='/home/airflow',
    directory='result',
    dag=dag
)

task_clear_result_directory = ClearDirectoryOperator(
    task_id='clear_result_directory',
    directory=get_local_result_directory(),
    pattern='*',
    dag=dag
)


# Get and unzip data -----------------------------------------------------


data_zip_file = 'hubway-data.zip'

task_get_data = BashOperator(
    task_id='get_data',
    bash_command='kaggle datasets download -p /home/airflow/hubway -d acmeyer/hubway-data',
    dag=dag
)

task_unzip_data = BashOperator(
    task_id='unzip_data',
    bash_command=f"unzip {get_local_import_directory()}/{data_zip_file} -d {get_local_import_directory()}",
    dag=dag,
)


# Copy data to hdfs ------------------------------------------------------


def create_hdfs_partition_task(filename, data_layer):
    date_str = get_date_str_from_file(filename)
    remote_target_directory = get_remote_data_directory(data_layer)

    print(f"Creating hdfs partition '{date_str}' for file '{filename}'")

    task_create_partition_directory = HdfsMkdirFileOperator(
        task_id=f'create_partition_directory_{date_str}_{data_layer}',
        directory=f'{remote_target_directory}/{date_str}',
        hdfs_conn_id='hdfs',
        dag=dag,
    )

    return task_create_partition_directory

def create_hdfs_result_partition_task():
    remote_target_directory = get_remote_data_directory('result')

    task_create_partition_directory = HdfsMkdirFileOperator(
        task_id='create_partition_directory_result',
        directory=remote_target_directory,
        hdfs_conn_id='hdfs',
        dag=dag
    )

    return task_create_partition_directory



# Put data into hdfs -----------------------------------------------------


def create_hdfs_put_file_task(filename, data_layer):
    date_str = get_date_str_from_file(filename)
    target_directory = get_local_import_directory()
    remote_target_directory = get_remote_data_directory(data_layer)

    print(f"Putting file '{filename}' into hdfs partition '{date_str}'")

    task_hdfs_put_file = HdfsPutFileOperator(
        task_id=f'put_hdfs_file_{filename.lower()}',
        local_file=f'{target_directory}/{filename}',
        remote_file=f'{remote_target_directory}/{date_str}/{filename}',
        hdfs_conn_id='hdfs',
        dag=dag,
    )

    return task_hdfs_put_file

def create_hdfs_put_kpi_file_task(date_str, data_layer):
    filename = f'kpis_{date_str}.csv'
    target_directory = get_local_kpi_directory(date_str)
    remote_target_directory = get_remote_data_directory(data_layer)

    print(f"Putting file '{filename}' into hdfs partition '{date_str}'")

    task_hdfs_put_file = HdfsPutFileOperator(
        task_id=f'put_hdfs_kpi_file_{filename.lower()}',
        local_file=f'{target_directory}/{filename}',
        remote_file=f'{remote_target_directory}/{date_str}/{filename}',
        hdfs_conn_id='hdfs',
        dag=dag,
    )

    return task_hdfs_put_file

def create_hdfs_put_result_file_task():
    target_file = 'result_kpis.csv'
    target_directory = get_local_result_directory()
    remote_target_directory = get_remote_data_directory('result')

    task_hdfs_put_file = HdfsPutFileOperator(
        task_id='put_hdfs_result_file',
        local_file=f'{target_directory}/{target_file}',
        remote_file=f'{remote_target_directory}/{target_file}',
        hdfs_conn_id='hdfs',
        dag=dag,
    )

    return task_hdfs_put_file


# Create SQL tables ------------------------------------------------------


task_synchronize = DummyOperator(
        task_id='synchronize',
        retries=3,
        dag=dag
    )

def create_sql_table_string(filename, data_layer):
    date_str = get_date_str_from_file(filename)
    comment = f'Trip-Data-{date_str}'

    remote_target_directory = get_remote_data_directory(data_layer)
    location = f'{remote_target_directory}/{date_str}'
    
    table_name = get_table_name_from_date_str(date_str)

    sql_create_table_string=f'''
        CREATE EXTERNAL TABLE IF NOT EXISTS {table_name}(
            trip_duration INT,
            start_time TIMESTAMP,
            stop_time TIMESTAMP,
            start_station_id INT,
            start_station_name STRING,
            start_station_latitude DECIMAL(8, 6),
            start_station_longitude DECIMAL(9, 6),
            end_station_id INT,
            end_station_name STRING,
            end_station_latitude DECIMAL(8, 6),
            end_station_longitude DECIMAL(9, 6),
            bike_id INT,
            user_type STRING,
            birth_year INT,
            gender INT
        ) COMMENT "{comment}"
        ROW FORMAT SERDE "org.apache.hadoop.hive.serde2.OpenCSVSerde"
        WITH SERDEPROPERTIES (
            "separatorChar" = ",",
            "quoteChar" = "\\""
        ) STORED AS TEXTFILE LOCATION "{location}"
        TBLPROPERTIES ("skip.header.line.count"="1")'''

    return sql_create_table_string

def create_sql_create_table_task(filename, data_layer):
    date_str = get_date_str_from_file(filename)
    sql_string = create_sql_table_string(filename, data_layer)

    task_sql_script = HiveOperator(
        task_id='create_table_' + date_str,
        hql=sql_string,
        hive_cli_conn_id='beeline',
        dag=dag
    )

    return task_sql_script


# Calculate KPIs ---------------------------------------------------------


def calc_average_trip_duration(date_str):
    table_name = get_table_name_from_date_str(date_str)
    target_directory = get_local_kpi_directory(date_str)

    sql_string = f'''
        SELECT AVG(trip_duration) AS avg_trip_duration
        FROM {table_name}
    '''

    task = HiveToCsvOperator(
        task_id='calc_average_trip_duration_' + table_name,
        hql=sql_string,
        hive_cli_conn_id='beeline',
        target_directory=target_directory,
        csv_file='kpi_trip_duration.csv',
        dag=dag
    )

    return task

def calc_average_trip_distance(date_str):
    table_name = get_table_name_from_date_str(date_str)
    target_directory = get_local_kpi_directory(date_str)

    sql_string = f'''
        SELECT (
        AVG(
	    SQRT(
	            (
                        71.5 * (start_station_longitude - end_station_longitude)
	                * (71.5 * (start_station_longitude - end_station_longitude))
                        + (111.3 * (start_station_latitude - end_station_latitude))
                        * (111.3 * (start_station_latitude - end_station_latitude))
                    )
                )
            )
        ) AS avg_trip_distance
        FROM {table_name}
    '''

    task = HiveToCsvOperator(
        task_id='calc_average_trip_distance_' + table_name,
        hql=sql_string,
        hive_cli_conn_id='beeline',
        target_directory=target_directory,
        csv_file='kpi_trip_distance.csv',
        dag=dag
    )

    return task


def calc_gender_share_percentage(date_str):
    table_name = get_table_name_from_date_str(date_str)
    target_directory = get_local_kpi_directory(date_str)

    sql_string = f'''
        SELECT gender, (
                COUNT(gender) * 100 / (
                SELECT COUNT(*) FROM {table_name})
            ) AS share
        FROM {table_name}
        GROUP BY gender
    '''

    task = HiveToCsvOperator(
        task_id='calc_gender_share_percentage_' + table_name,
        hql=sql_string,
        hive_cli_conn_id='beeline',
        target_directory=target_directory,
        csv_file='kpi_gender_share.csv',
        dag=dag
    )

    return task

def calc_age_share_percentage(date_str):
    table_name = get_table_name_from_date_str(date_str)
    target_directory = get_local_kpi_directory(date_str)

    sql_string = f'''
        SELECT 
            (YEAR(CURRENT_DATE) - birth_year) AS age,
            (COUNT(birth_year) * 100 / (
                SELECT COUNT(*) FROM {table_name})
            ) AS share
        FROM {table_name}
        GROUP BY birth_year
    '''

    task = HiveToCsvOperator(
        task_id='calc_age_share_percentage_' + table_name,
        hql=sql_string,
        hive_cli_conn_id='beeline',
        target_directory=target_directory,
        csv_file='kpi_age_share.csv',
        dag=dag
    )

    return task

def calc_top_10_bikes(date_str):
    table_name = get_table_name_from_date_str(date_str)
    target_directory = get_local_kpi_directory(date_str)

    sql_string = f'''
        SELECT bike_id, (COUNT(*) - 1) AS count
        FROM {table_name}
        GROUP BY bike_id
        ORDER BY COUNT(*) DESC
        LIMIT 10
    '''
    
    task = HiveToCsvOperator(
        task_id='calc_top_10_bikes_' + table_name,
        hql=sql_string,
        hive_cli_conn_id='beeline',
        target_directory=target_directory,
        csv_file='kpi_top_10_bikes.csv',
        dag=dag
    )

    return task

def calc_top_10_start_stations(date_str):
    table_name = get_table_name_from_date_str(date_str)
    target_directory = get_local_kpi_directory(date_str)

    sql_string = f'''
        SELECT start_station_id, start_station_name, (COUNT(*)-1) AS count
        FROM {table_name}
        GROUP BY start_station_id, start_station_name
        ORDER BY COUNT(*) DESC
        LIMIT 10
    '''

    task = HiveToCsvOperator(
        task_id='calc_top_10_start_stations_' + table_name,
        hql=sql_string,
        hive_cli_conn_id='beeline',
        target_directory=target_directory,
        csv_file='kpi_top_10_start_stations.csv',
        dag=dag
    )

    return task

def calc_top_10_end_stations(date_str):
    table_name = get_table_name_from_date_str(date_str)
    target_directory = get_local_kpi_directory(date_str)

    sql_string = f'''
        SELECT end_station_id, end_station_name, (COUNT(*)-1) AS count
        FROM {table_name}
        GROUP BY end_station_id, end_station_name
        ORDER BY COUNT(*) DESC
        LIMIT 10
    '''

    task = HiveToCsvOperator(
        task_id='calc_top_10_end_stations_' + table_name,
        hql=sql_string,
        hive_cli_conn_id='beeline',
        target_directory=target_directory,
        csv_file='kpi_top_10_end_stations.csv',
        dag=dag
    )

    return task

def calc_usage_share_per_time_slot(date_str):
    table_name = get_table_name_from_date_str(date_str)
    target_directory = get_local_kpi_directory(date_str)

    sql_string = f'''
        SELECT
        (timeslot1 * 100 / (SELECT COUNT(*) FROM {table_name})) AS usage_share_between_00_and_06,
        (timeslot2 * 100 / (SELECT COUNT(*) FROM {table_name})) AS usage_share_between_06_and_12,
        (timeslot3 * 100 / (SELECT COUNT(*) FROM {table_name})) AS usage_share_between_12_and_18,
        (timeslot4 * 100 / (SELECT COUNT(*) FROM {table_name})) AS usage_share_between_18_and_00
    FROM
        (SELECT COUNT(*) AS timeslot1 FROM {table_name} WHERE SUBSTR(start_time, 12) BETWEEN '00:00:00' AND '06:00:00') query_timeslot_1,
        (SELECT COUNT(*) AS timeslot2 FROM {table_name} WHERE SUBSTR(start_time, 12) BETWEEN '06:00:00' AND '12:00:00') query_timeslot_2,
        (SELECT COUNT(*) AS timeslot3 FROM {table_name} WHERE SUBSTR(start_time, 12) BETWEEN '12:00:00' AND '18:00:00') query_timeslot_3,
        (SELECT COUNT(*) AS timeslot4 FROM {table_name} WHERE SUBSTR(start_time, 12) BETWEEN '18:00:00' AND '23:59:59') query_timeslot_4
    '''

    task = HiveToCsvOperator(
            task_id='calc_usage_share_per_time_slot_' + table_name,
        hql=sql_string,
        hive_cli_conn_id='beeline',
        target_directory=target_directory,
        csv_file='kpi_usage_share_per_time_slot.csv',
        dag=dag
    )

    return task



# KPI collection ---------------------------------------------------------


def create_collect_kpi_file_task(target_directory, date_str):
    source_directory = get_local_kpi_directory(date_str)
    filename = f'kpis_{date_str}.csv'

    total_path = f'{source_directory}/{filename}'
    target_path = f'{target_directory}/{filename}'

    task = CopyFileOperator(
        task_id=f'copy_file_{filename}',
        source=total_path,
        dest=target_path,
        overwrite=True,
        dag=dag
    )

    return task


# Combine CSV files ------------------------------------------------------


def create_combine_csv_files_task(date_str):
    task = CombineCsvOperator(
        task_id=f'combine_csv_{date_str}',
        source_directory=get_local_kpi_directory(date_str),
        csv_file=f'kpis_{date_str}.csv',
        dag=dag
    )

    return task


# Append CSV files -------------------------------------------------------


def create_append_csv_files_task(source_directory):
    task = AppendCsvOperator(
        task_id='append_kpi_csv_files',
        source_directory=source_directory,
        csv_file='result_kpis.csv',
        dag=dag
    )

    return task


# Scheduling -------------------------------------------------------------

schedule_mode = 'debug'
schedule_task_count = 3

def schedule_calc_kpis(files):
    i = 0
    for filename in files:
        if schedule_mode == 'debug' and i == schedule_task_count:
            break
        
        date_str = get_date_str_from_file(filename)
        
        task_hdfs_partition = create_hdfs_partition_task(filename, 'raw')
        task_hdfs_put = create_hdfs_put_file_task(filename, 'raw')
        task_sql_script = create_sql_create_table_task(filename, 'raw')

        task_calc_average_trip_duration = calc_average_trip_duration(date_str)
        task_calc_average_trip_distance = calc_average_trip_distance(date_str)
        task_calc_gender_share_percentage = calc_gender_share_percentage(date_str)
        task_calc_age_share_percentage = calc_age_share_percentage(date_str)
        task_calc_top_10_bikes = calc_top_10_bikes(date_str)
        task_calc_top_10_start_stations = calc_top_10_start_stations(date_str)
        task_calc_top_10_end_stations = calc_top_10_end_stations(date_str)
        task_calc_usage_share_per_time_slot = calc_usage_share_per_time_slot(date_str)
        
        task_combine_csv_files = create_combine_csv_files_task(date_str)


        task_hdfs_partition_final = create_hdfs_partition_task(date_str, 'final')
        task_hdfs_put_final = create_hdfs_put_kpi_file_task(date_str, 'final')
        
        result_directory = get_local_result_directory()
        task_collect_kpi_file = create_collect_kpi_file_task(result_directory, date_str)
        
        
        task_append_csv_files = create_append_csv_files_task(result_directory)
        task_create_result_partition = create_hdfs_result_partition_task()
        task_hdfs_put_result_file = create_hdfs_put_result_file_task()

        task_unzip_data >> task_hdfs_partition >> task_hdfs_put >> task_sql_script >> task_calc_average_trip_duration >> task_calc_average_trip_distance >> task_calc_gender_share_percentage >> task_calc_age_share_percentage >> task_calc_top_10_bikes >> task_calc_top_10_start_stations >> task_calc_top_10_end_stations >> task_calc_usage_share_per_time_slot >> task_combine_csv_files >> task_hdfs_partition_final >> task_hdfs_put_final >> task_collect_kpi_file >> task_synchronize >> task_append_csv_files >> task_create_result_partition >> task_hdfs_put_result_file
        i += 1


schedule_calc_kpis(relevant_data_files)

# Execute tasks ----------------------------------------------------------

task_create_import_directory >> task_clear_import_directory >> task_get_data
task_create_kpi_directory >> task_clear_kpi_directory >> task_get_data
task_create_result_directory >> task_clear_result_directory >> task_get_data
task_get_data >> task_unzip_data


