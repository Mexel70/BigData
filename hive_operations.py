from airflow.plugins_manager import AirflowPlugin
from operators.hive_to_csv_operator import *
from operators.combine_csv_operator import *
from operators.append_csv_operator import *

class HivePlugin(AirflowPlugin):  
    name = "hive_operations"
    operators = [HiveToCsvOperator, CombineCsvOperator, AppendCsvOperator]
    hooks = []
    executors = []
    macros = []
    admin_views = []
    flask_blueprints = []
    menu_links = []

