from airflow.exceptions import AirflowException
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

import os


class CsvColumn():

    template_fields = ('header', 'values')

    def __init__(
            self,
            header,
            values,
            *args, **kwargs):

        self.header = header
        self.values = values

    def get_header(self):
        return self.header

    def get_values(self):
        return self.values

    def get_value(self, row):
        return self.values[row]

    def append_value(self, value):
        self.values.append(value)


class CsvInfo():

    template_fields = ('columns',)

    def __init__(
            self,
            columns,
            *args, **kwarags):

        self.columns = columns

    def append_column(self, column):
        self.columns.append(column)

    def get_columns(self):
        return self.columns

    def calc_max_column_value_count(self):
        max_length = 0
        for column in self.columns:
            length = len(column.get_values())
            if length > max_length:
                max_length = length

        return max_length

    def write_to_file(self, file_path):
        target_file = open(file_path, "w")
        max_rows = self.calc_max_column_value_count()
        
        max_columns = len(self.columns)
        counter = 1

        for column in self.columns:
            header = column.get_header().replace("\n", "")
            target_file.write(header)

            if counter != max_columns:
                target_file.write(",")

            counter += 1
        
        target_file.write("\n")
        counter = 1

        for i in range(0, max_rows):
            for column in self.columns:

                if i < len(column.get_values()):
                    value = column.get_value(i).replace("\n", "")
                    target_file.write(value)

                if counter != max_columns:
                    target_file.write(",")

                counter += 1

            target_file.write("\n")

        target_file.close()


class CombineCsvOperator(BaseOperator):

    template_fields = ('source_directory', 'csv_file',)
    ui_color = '#36938'

    @apply_defaults
    def __init__(
            self,
            source_directory,
            csv_file,
            *args, **kwargs):

        super(CombineCsvOperator, self).__init__(*args, **kwargs)
        self.source_directory = source_directory
        self.csv_file = csv_file

    def read_csv_file(self, csv_file_path):
        csv_file = open(csv_file_path, "r")
        
        columns = []
        first_line = csv_file.readline()

        header = first_line.split(",")
        for h in header:
            columns.append(CsvColumn(header=h, values=[]))
        
        column_count = len(columns)
        for line in csv_file:
            values = line.split(",")
            for i in range(0, column_count):
                if i < len(values):
                    columns[i].append_value(values[i])
                else:
                    break

        csv_file.close()
        return CsvInfo(header=header, columns=columns)

    def combine_csv_files(self, csv_infos):
        result_csv = CsvInfo(columns=[])

        for csv_info in csv_infos:
            for column in csv_info.get_columns():
                result_csv.append_column(column)

            # insert empty columns in between
            empty_column = CsvColumn(header="", values=[])
            result_csv.append_column(empty_column)

        return result_csv

    def execute(self, context):
        self.log.info("CombineCsvOperator execution started.")

        if not os.path.isdir(self.source_directory):
            raise AirflowException(f"Directory '{self.source_directory} does not exist.")
        
        csv_infos = []
        for r, d, f in os.walk(self.source_directory):
            for filename in f:
                total_path = os.path.join(r, filename)
                csv_info = self.read_csv_file(total_path)
                csv_infos.append(csv_info)
        
        result = self.combine_csv_files(csv_infos)
        result.write_to_file(f'{self.source_directory}/{self.csv_file}')

        self.log.info("CombineCsvOperator done.")





















