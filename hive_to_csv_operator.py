from airflow.exceptions import AirflowException
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.operators.hive_operator import *
from airflow.hooks.hive_hooks import HiveServer2Hook

import os

class HiveToCsvOperator(HiveOperator):

    template_fields = ('target_directory', 'csv_file',)
    ui_color = '#585858'

    @apply_defaults
    def __init__(
            self,
            target_directory,
            csv_file,
            *args, **kwargs):

        super(HiveToCsvOperator, self).__init__(*args, **kwargs)
        self.target_directory = target_directory
        self.csv_file = csv_file

    def execute(self, context):

        self.log.info("HiveToCsvOperator execution started.")
        self.hook = HiveServer2Hook(self.hive_cli_conn_id)

        if not os.path.isdir(self.target_directory):
            os.makedirs(self.target_directory)

        self.hook.to_csv(
            hql=self.hql,
            csv_filepath=f'{self.target_directory}/{self.csv_file}',
            schema='default',
            delimiter=',',
            lineterminator='\r\n',
            output_header=True,
            fetch_size=1000,
            hive_conf=None
        )

        self.log.info("HiveToCsvOperator done.")

