#!/bin/sh

cp /home/airflow/airflow/dags/hubway.py /home/airflow/repos/BigData/
cp /home/airflow/airflow/plugins/hive_operations.py /home/airflow/repos/BigData/
cp /home/airflow/airflow/plugins/operators/hive_to_csv_operator.py /home/airflow/repos/BigData/
cp /home/airflow/airflow/plugins/operators/combine_csv_operator.py /home/airflow/repos/BigData/
cp /home/airflow/airflow/plugins/operators/append_csv_operator.py /home/airflow/repos/BigData/

cd repos/BigData/
git status
git add .
git commit -m "Updates"
git push origin master
