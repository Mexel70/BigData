from airflow.plugins_manager import AirflowPlugin
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.exceptions import AirflowException

import os

class CsvAppender():

    def __init__(
            self,
            *args, **kwargs):

        self.lines = []

    def append_line(self, line):
        value = line.replace("\n", "")
        self.lines.append(value)

    def next_csv(self):
        self.lines.append("\n")

    def write_to_file(self, file_path):

        f = open(file_path, "w")

        for line in self.lines:
            f.write(line + "\n")

        f.close()

class AppendCsvOperator(BaseOperator):

    template_fields = ('source_directory', 'csv_file',)
    ui_color = '#94F83'

    @apply_defaults
    def __init__(
            self,
            source_directory,
            csv_file,
            *args, **kwargs):

        super(AppendCsvOperator, self).__init__(*args, **kwargs)
        self.source_directory = source_directory
        self.csv_file = csv_file

    def execute(self, context):

        self.log.info("AppendCsvOperator execution started.")
        
        appender = CsvAppender()
        for r, d, f in os.walk(self.source_directory):
            for filename in f:
                total_path = os.path.join(r, filename)

                f = open(total_path, "r")
                
                appender.append_line(filename)
                for line in f:
                    appender.append_line(line)

                appender.next_csv()
                f.close()

        appender.write_to_file(f'{self.source_directory}/{self.csv_file}')

        self.log.info("AppendCsvOperator done.")

